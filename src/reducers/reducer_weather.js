import { FETCH_WEATHER } from '../actions';

export default function(state = [], action) {
    let c = 0;
    switch(action.type) {
        case FETCH_WEATHER:
            state.forEach((t,i) => {
                if(t.city.id === action.payload.data.city.id) {
                    c = 1;
                }
            });
            if(c == 1)
              return state;
            return [ action.payload.data, ...state ];
    }
    return state;
}