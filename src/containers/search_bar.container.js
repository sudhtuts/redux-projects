import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { fetchWeather } from '../actions/index';

class SearchBar extends Component {

    constructor(props) {
        super(props);

        this.state = { term: '' };

        this.onInputChange = this.onInputChange.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
    }

    onInputChange(evt) {
        let term = evt.target.value;
        this.setState({term});
    }

    onSubmit(evt) {
        evt.preventDefault();
        this.props.fetchWeather(this.state.term);
        this.setState({term: ''});
    }

    render() {
        return (
            <form onSubmit={this.onSubmit} className="input-group">
                <input 
                  placeholder="Get the 5 day weather details..." 
                  className="form-control"
                  value={this.state.term}
                  onChange={this.onInputChange}/>
                <span className="input-group-btn">
                    <button
                      disabled={this.state.term === ''}
                      type="submit"
                      className="btn btn-secondary">
                        Go!
                    </button>
                </span>
            </form>
        )
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({fetchWeather}, dispatch);
}

export default connect(null, mapDispatchToProps)(SearchBar);