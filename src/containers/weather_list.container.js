import React, { Component } from 'react';
import { connect } from 'react-redux';
import Chart from '../components/chart';
import GoogleMap from '../components/google-map';

class WeatherList extends Component {

    constructor(props) {
        super(props);
        this.state = { weather: [] }
    }

    renderWeather(cityData) {

        const name = cityData.city.name;
        const temps = cityData.list.map(t => t.main.temp);
        const humidities = cityData.list.map(t => t.main.humidity);
        const pressures = cityData.list.map(t => t.main.pressure);

        const { lat,lon } = cityData.city.coord;

        return (
            <tr key={Math.random()}>
                <td><GoogleMap lon={lon} lat={lat} /></td>
                <td>
                    <Chart data={temps} color="orange" />
                </td>
                <td>
                    <Chart data={humidities} color="#147CE0" />
                </td>
                <td>
                    <Chart data={pressures} color="green" />
                </td>
                
            </tr>
        )
    }

    render() {
        return (
            <table className="table table-hover">
                <thead>
                    <tr>
                        <th>City</th>
                        <th>Temperature (K)</th>
                        <th>Pressure (hPa)</th>
                        <th>Humidity (%)</th>
                    </tr>
                </thead>
                <tbody>
                    {this.props.weather.map(this.renderWeather)}
                </tbody>
            </table>
        )
    }
}

function mapStateToProps({ weather }) {
    return { weather }
}

export default connect(mapStateToProps)(WeatherList);