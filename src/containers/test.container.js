import React, { Component } from 'react';
import { connect } from 'react-redux';


class TestComponent extends Component {

    constructor(props) {
        super(props);
        
    }

    render() {
        return (
           <p>Hi!! Test</p>
        )
    }
}

function mapStateToProps(data) {
    console.log("test got props",data);
    return {};
}

export default connect(mapStateToProps)(TestComponent);